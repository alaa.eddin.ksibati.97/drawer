export class ShapeBuilder {
  constructor() {}
  make(shape) {
    shape.prepare();
    shape.setName();
    shape.setColor();
    shape.setPoints();
    return shape.setTotal(); // return shape instance
  }
}
