export class ShapeBuilderInterface {
  constructor() {}
  prepare() {}
  setName() {}
  setColor() {}
  setPoints() {}
  setTotal() {}
}
