export class Shape {
  constructor() {
    this.name = '';
    this.total = 0;
    this.color = '';
    this.points = [];
  }
  getTotal() {
    return this.total;
  }
  getName() {
    return this.name;
  }
  getColor() {
    return this.color;
  }
  getPoints() {
    return this.points;
  }

  draw() {
    // drawing the final points logic will goes here
    console.log(`result is ${this.name}: ${this.total}`);
  }
}
