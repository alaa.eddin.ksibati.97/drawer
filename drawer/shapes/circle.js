import { Shape } from '../shape.js';
import { ShapeBuilderInterface } from '../shape-builder/shape-builder-interface.js';
export class Circle extends ShapeBuilderInterface {
  #shape;
  constructor(r, color) {
    super();
    this.r = r;
    this.color = color;
  }
  prepare() {
    this.#shape = new Shape();
  }
  setName() {
    this.#shape.name = this.constructor.name;
    return this.#shape;
  }
  setColor() {
    this.#shape.color = this.color;
    return this.#shape;
  }
  setPoints() {
    // the logic of creating the points goes here
    this.#shape.points = [];
    return this.#shape;
  }
  setTotal() {
    this.#shape.total = Math.PI * this.r * 2;
    return this.#shape;
  }
}
