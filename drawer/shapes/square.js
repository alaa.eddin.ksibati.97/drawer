import { Shape } from '../shape.js';
import { ShapeBuilderInterface } from '../shape-builder/shape-builder-interface.js';
export class Square extends ShapeBuilderInterface {
  #shape;
  constructor(n,color) {
    super();
    this.n = n;
    this.color = color;
  }
  // the logic of creating the points goes here

  prepare() {
    this.#shape = new Shape();
  }
  setName() {
    this.#shape.name = this.constructor.name;
    return this.#shape;
  }
  setColor() {
    this.#shape.color = this.color;
    return this.#shape;
  }
  setPoints() {
    // the logic of creating the points goes here
    this.#shape.points = [];
    return this.#shape;
  }
  setTotal() {
    this.#shape.total = this.n * 4;
    return this.#shape;
  }
}
