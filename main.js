import { ShapeBuilder } from './drawer/shape-builder/index.js';
import { Drawer } from './drawer/index.js';
import { Circle } from './drawer/shapes/circle.js';
import { Square } from './drawer/shapes/square.js';

let sb = new ShapeBuilder();
let shape1 = sb.make(new Circle(5, '#f00'));
let shape2 = sb.make(new Square(5, '#0f0'));
// depend on shape class
shape1.draw();
shape2.draw();
// or using independent drawer class
let print = new Drawer();
print.draw(shape1.getTotal());
print.draw(shape2.getTotal());
