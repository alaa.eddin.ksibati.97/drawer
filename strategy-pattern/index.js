export class Circle {
  constructor(r) {
    this.r = r;
  }
  draw() {
    console.log(`draw circle with r: ${this.r}`);
  }
}
export class Square {
  constructor(n) {
    this.n = n;
  }
  draw() {
    console.log(`draw Square with n: ${this.n}`);
  }
}

export class drawStrategy {
  constructor() {
    this.strategy = null;
  }
  setStrategy(strategy) {
    this.strategy = strategy;
  }
  draw() {
    if (this.strategy) this.strategy.draw();
    else throw Error('set your strategy first!');
  }
}

let c = new Circle(5);
let s = new Square(5);
let d = new drawStrategy();
d.setStrategy(c);
d.draw();
d.setStrategy(s);
d.draw();
